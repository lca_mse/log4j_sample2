// Copyright (C) logi.cals automation solutions and services GmbH. All rights reserved.

package com.lca_ms.samples;

// *****************************************************************************
// Imports
// *****************************************************************************
// import java.util.*;
// import org.apache.log4j.*;
import java.util.logging.Logger;

/**
 * Hello world!
 */
public final class App
{

  // das ist sicher keine constante! deswegen NICHT in grossbuchstaben. Egal was checkstyle sagt.
  // CHECKSTYLE:OFF
  private static final Logger logger = Logger.getLogger(App.class.getName());
  // CHECKSTYLE:ON

  private App()
  {
  }

  @SuppressWarnings("PMD.SystemPrintln")
  public static void main(String[] args)
  {
    System.out.println("Hello World!");
    logger.info(System.getProperty("java.library.path"));
  }

}
